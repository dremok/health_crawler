import time
from operator import itemgetter
from threading import Thread

from flask import Flask
from flask import request

from web_server.server_database import Database

HTML_END = '</body></html>'

HEAD = '<html><head><title>NHS search </title></head>'
MAIN_BODY = '<body>' \
            '<h1>Welcome to the NHS search engine.</h1>' \
            '</br></br>' \
            '<form action="/search">' \
            'Search: <input type="text" name="search_phrase">' \
            '<input type="submit" value="Search">' \
            '</form>'


class LoadingThread(Thread):
    """Runs in the background and updates the indexes with scraped pages. """

    def __init__(self, db):
        super().__init__()
        self.database = db

    def run(self):
        while True:
            self.database.index()
            time.sleep(10)


app = Flask(__name__)

database = Database()
database.load_from_json()
loading_thread = LoadingThread(database)
loading_thread.start()


@app.route("/", methods=['GET'])
def welcome():
    return HEAD + MAIN_BODY + "</br>Cache size = " + str(len(database.cache)) \
           + "</br>Indexed words = " + str(len(database.inverse_content_dictionary)) + HTML_END


@app.route("/search", methods=['GET'])
def search():
    search_phrase = request.args.get('search_phrase')
    search_result = database.search(search_phrase)

    # Group results by title
    title_grouped_result = {}
    for key, value in search_result.items():
        title = database.cache[key]['title']
        if title not in title_grouped_result:
            title_grouped_result[title] = []
        title_grouped_result[title].append((key, value))

    # Compute score for each title and find URL with max score
    final_result = []
    for title, urls_with_title in title_grouped_result.items():
        score_sum = 0
        max_score = 0
        max_url = ""
        for entry in urls_with_title:
            score_sum += entry[1]
            if entry[1] > max_score:
                max_score = entry[1]
                max_url = entry[0]
        final_result.append((max_url, score_sum / len(urls_with_title)))

    final_result.sort(key=itemgetter(1), reverse=True)

    # Filter out results that are too irrelevant
    if len(final_result) > 0:
        max_score = final_result[0][1]
        for i in range(1, len(final_result)):
            if final_result[i][1] < 0.05 * max_score:
                final_result = final_result[0:i]
                break

    if not final_result:
        return html_result('</br><h3>No result</h3>')

    formatted_result = list(map(lambda result_tuple: '<a href=' + result_tuple[0] + '>' +
                                                     database.cache[result_tuple[0]]['title'] + '</a>', final_result))
    return html_result('</br>'.join(formatted_result))


def html_result(html):
    return HEAD + MAIN_BODY + html + HTML_END


if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=True)
