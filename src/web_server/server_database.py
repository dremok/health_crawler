import json
import re
import time

import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer

wordnet_lemmatizer = WordNetLemmatizer()

COMPLETE_JSON_FILE = 'cache_complete.json'


class Database:
    """Reads from the json cache and populates inverse indexes from the scraped pages."""
    def __init__(self):
        self.cache = {}
        self.indexed_urls = set()
        self.inverse_content_dictionary = {}
        self.df_content_dictionary = {}
        self.inverse_title_dictionary = {}
        self.df_title_dictionary = {}

    def index(self):
        """Indexes scraped pages."""
        urls_to_index = set(self.cache.keys()).difference(self.indexed_urls)
        for url in urls_to_index:
            entry = self.cache[url]
            self.add_entry(url, entry['title'], entry['content'])
            self.indexed_urls.add(url)

    def load_from_json(self):
        cache_is_loaded = False
        while not cache_is_loaded:
            try:
                with open(COMPLETE_JSON_FILE, 'r') as infile:
                    self.cache = json.load(infile)
                    cache_is_loaded = True
            except IOError:
                time.sleep(1)
                # Retry

    def add_entry(self, url, title, content):
        """Populates the inverse indexes with the content of the scraped pages"""
        populate_indexes(url, content, self.inverse_content_dictionary, self.df_content_dictionary)
        populate_indexes(url, title, self.inverse_title_dictionary, self.df_title_dictionary)

    def search(self, search_phrase):
        """Searches the database and returns a ranked result of all pages that match the provided phrase."""
        if search_phrase is None:
            return []
        result = {}

        tokens = process_text(search_phrase)

        results_per_token = []

        for token in tokens:
            if token in self.inverse_content_dictionary.keys():
                results_per_token.append(list(self.inverse_content_dictionary[token].keys()))
            else:
                results_per_token.append([])
            # Hits in title and content are arbitrarily weighted in a way that seemed to work well for this domain.
            score(token, 1, self.inverse_content_dictionary, self.df_content_dictionary, result)
            score(token, 10, self.inverse_title_dictionary, self.df_title_dictionary, result)

        # Filter out results that do not contain all (existing) words
        for urls in results_per_token:
            result = {url: result[url] for url in result.keys() if url in urls}

        return result


def populate_indexes(url, text, inverse_dictionary, df_dictionary):
    """Processes the content of scraped pages and updates inverse indexes."""
    tokens = process_text(text)

    for token in tokens:
        if token not in inverse_dictionary.keys():
            inverse_dictionary[token] = {}
        if url in inverse_dictionary[token].keys():
            inverse_dictionary[token][url] += 1
        else:
            inverse_dictionary[token][url] = 1

    for token in set(tokens):
        if token in df_dictionary.keys():
            df_dictionary[token] += 1
        else:
            df_dictionary[token] = 1


def process_text(text):
    """Basic normalization, tokenization and lemmatization."""
    normalized_text = text.lower()
    normalized_text = re.sub(re.compile('[\n\t\r]'), ' ', normalized_text)
    normalized_text = re.sub(re.compile('[^a-zA-Z0-9_\s]+'), '', normalized_text)
    tokens = nltk.word_tokenize(normalized_text)
    tokens = [token for token in tokens if token not in stopwords.words('english')]
    tokens = [wordnet_lemmatizer.lemmatize(token) for token in tokens]
    return tokens


def score(token, weight, inverse_dictionary, df_dictionary, result):
    """Give score to each web page that contains the token according to a simple TF-IDF heuristic."""
    if token in inverse_dictionary.keys():
        documents_with_token = inverse_dictionary[token]
        for key, value in documents_with_token.items():
            if key in result.keys():
                result[key] += weight * value / df_dictionary[token]
            else:
                result[key] = weight * value / df_dictionary[token]
