from scraping.scraper import Scraper


def main():
    scraper = Scraper('http://www.nhs.uk/Conditions/Pages/hub.aspx')
    scraper.scrape('nhs.uk/[Cc]onditions/')


if __name__ == '__main__':
    main()
