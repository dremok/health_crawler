import json
import os
import time

JSON_FILE = 'cache.json'
COMPLETE_JSON_FILE = 'cache_complete.json'


class Cache:
    """A representation of the cache of scraped web pages."""
    def __init__(self):
        self.cache = {}

    def cache_entry(self, url, title, content):
        """Caches the url, title and content of a web page and stores it in a json file."""
        if url in self.cache.keys():
            return False
        self.cache[url] = {'title': title, 'content': content}
        with open(JSON_FILE, 'w') as outfile:
            json.dump(self.cache, outfile)
        try:
            # A copy of the written cached is held in order for the web service to
            # read from the complete cache while the scraper is writing a new entry.
            os.remove(COMPLETE_JSON_FILE)
            os.rename(JSON_FILE, COMPLETE_JSON_FILE)
        except IOError:
            pass
        return True

    def load_from_json(self):
        """Loads the cache from file when resuming scraping."""
        cache_is_loaded = False
        while not cache_is_loaded:
            try:
                with open(COMPLETE_JSON_FILE, 'r') as infile:
                    self.cache = json.load(infile)
                    cache_is_loaded = True
            except IOError:
                time.sleep(1)
                # Retry
