import json
import re
from collections import deque

import urllib3
from bs4 import BeautifulSoup

from scraping.scraper_cache import Cache


class Scraper:
    """A basic scraper that starts crawling at the specified URL and scrapes linked pages."""

    def __init__(self, start_url):
        self.database = Cache()
        self.database.load_from_json()
        self.http = urllib3.PoolManager()
        self.scraped_links = set(self.database.cache.keys())
        try:
            with open('crawl_queue.json', 'r') as infile:
                self.crawl_queue = deque(json.load(infile))
        except IOError:
            self.crawl_queue = deque(self.database.cache.keys())
        self.crawl_queue.append(start_url)

    def save_crawl_queue(self):
        """Saves the queue of links to crawl to a json file."""
        with open('crawl_queue.json', 'w') as outfile:
            json.dump(list(self.crawl_queue), outfile)

    def scrape(self, valid_link_regex):
        """Starts crawling at the specified URL and scrapes all linked pages.
        Each page with a URL that matches the regex argument will be scraped.
        """
        count = 0
        while True:
            if len(self.crawl_queue) > 0:
                count += 1
                if count % 1000 == 0:
                    self.save_crawl_queue()
                url = self.crawl_queue.pop()
                try:
                    request = self.http.request('GET', url)
                    code = request.status
                    if code != 200:
                        continue

                    content = request.data.decode('utf-8')
                    soup = BeautifulSoup(content, 'html.parser')
                    entry = create_entry(url, soup)
                    if self.database.cache_entry(url, entry['title'], entry['content']):
                        print(url)

                    links = soup.findAll('a', href=re.compile(valid_link_regex))
                    for link in links:
                        link_url = link.get('href').lower()
                        try:
                            anchor_index = link_url.find('#')
                            if anchor_index > -1:
                                link_url = link_url[0:anchor_index]
                            if link_url not in self.scraped_links:
                                self.crawl_queue.append(link_url)
                                self.scraped_links.add(link_url)
                        except AttributeError:
                            continue
                except BaseException as e:
                    print('Could not crawl ' + url)
                    print('Reason: ' + str(e))
            else:
                exit()


def create_entry(url, soup):
    """Extract title and content from a web page."""
    headers = soup.find_all('h1')
    if len(headers) == 0:
        return ""
    title = headers[0].get_text()
    content_array = soup.findAll(attrs={'class': re.compile(r"(main-content|article)")})
    content = ""
    for text in content_array:
        content += text.get_text() + "\n"
    return {'url': url, 'title': title, 'content': content}
